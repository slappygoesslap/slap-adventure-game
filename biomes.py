desert = {'name':'Desert'}
sea = {'name':'Sea'}
plains = {'name':'Plains'}
hills = {'name':'Hills'}
mountains = {'name':'Mountains'}
class unknownBiome(Exception):
    pass

def checkBiome(biome:int):
    if biome == '1':
        return str('Desert')
        
    if biome == '2':
        return str('Plains')
        
    if biome == '3':
        return str('Hills')
        
    if biome == '4':
        return str('Mountains')
        
    if biome == '5':
        return str('Sea')

        
water = 20
food = 20
health = 20
x = 3
y = 3
inventory = ['Sword']
class InvalidDirection(Exception):
    pass
def move(direction:str, map):
    global water
    global food
    global x
    global y  
    if direction == 'north':
        if y==0:
            pass
        else:
            y -= 1
            water -= 1
            food -=1
    elif direction == 'south':
        if y==len(map[0]) - 1:
            pass
        else:
            y += 1
            water -= 1
            food -=1
    elif direction == 'east':
        if x==len(map) - 1:
            pass
        else:
            x += 1
            water -= 1
            food -=1
    elif direction == 'west':
        if x ==0:
            pass
        else:
            x -= 1
            water -= 1
            food -=1
    else:
        raise InvalidDirection('Invalid direction given'+direction)
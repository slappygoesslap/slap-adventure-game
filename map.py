
import random
import player

class Biomes:
    def __init__(self, name):
        self.name = name
    def getWater(self,waterVar):
        if waterVar >= 20:
            if self.name != 'Desert':
                waterVar = 20
                print('You do not have enough room to carry all the water, you have picked up all the water you can.')
            else:  
                if self.name == 'Desert':
                    print('You cannot get water from the desert without a modification.')
                elif self.name == 'Sea':
                    waterVar = 20
                    print('You have maxed out your water because you are on a lake/in the sea.')
                else:
                    waterVar += 3
                    print('in water')
                    print('You collect 3 water.')
    def getFood(self, foodVar):
        if foodVar >= 20:
            print('You do not have enough room to carry all the food you\'ve collected. You carry all you can.')
            foodVar = 20
        else:
            print('in food')
            print('You collect 3 food.')
            foodVar += 3
            

    
x = 3
y = 3
class gameMap:
    def generateMap(x,y):
        array = []
        for a in range(0, x):
            array.append([])
            for b in range(0, y):
                array[a].append('*')
        return array


    def generateAnimals(x:int,y:int,array:list,map:list):
        x_size = len(map)
        y_size = len(map[0])
        for e in range(0,y_size):
            for f in range(0,x_size):
                animal = random.randint(0,len(array)-1)
                map[f][e] = str(animal)

        return map

    def printMap(map):
        x_size = len(map)
        y_size = len(map[0])
        for c in range(0, y_size):
            for d in range(0, x_size):
                if player.y == c:
                    if player.x == d:
                        print('p' + '   ', end = '')
                    else:
                        ch = map[d][c]
                        print(str(ch) + '   ', end = '')

                else:
                    ch = map[d][c]
                    print(str(ch) + '   ', end = '')
            print('\n')


    def randomizeMap(map,x,y):
        
        x_size = len(map)
        y_size = len(map[0])
        for e in range(0,y_size):
            for f in range(0,x_size):
                #keep note for map bug
                ch = map[f][e]
                biome = random.randint(x,y)
                map[f][e] = str(biome)

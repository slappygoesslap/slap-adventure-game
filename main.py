import map
import player
import biomes
import sys
import random
player.water = 20
player.food = 20
animalsList = ['Cow','Pig','Sheep','Chicken','Llama','Zombie','Boar','Wolf']
hostiles=['Zombie','Boar','Wolf']
grid = map.gameMap.generateMap(10,10)
map.gameMap.randomizeMap(grid,1,5)
animalArray = map.gameMap.generateMap(10,10)
animals = map.gameMap.generateAnimals(10,10,animalsList,animalArray)
onSea = True
animalCycle = 1

while True:
    while onSea == True:
        if biomes.checkBiome(grid[player.x][player.y]) == 'Sea' or biomes.checkBiome(grid[player.x][player.y]) == 'Mountains':
            if player.x == len(grid) - 1:
                pass
            else:
                player.x +=1
        else:
            onSea = False
    if animalCycle == 5:
        animals = map.gameMap.generateAnimals(10,10,animalsList,animalArray)
        animalCycle = 0
    
    ac1 = int(animals[player.x][player.y])
    ac2 = animalsList[ac1]
    if ac1 == 0:
        ac2 = 'None'
    map.gameMap.printMap(grid)
    print('----------------------------------------------------------------')
    print('Location: ',str(player.x)+' '+str(player.y))
    print('Biome: '+biomes.checkBiome(grid[player.x][player.y]))
    print('Water: '+str(player.water))
    print('Food: '+str(player.food))
    if ac2 == 'None':
        print('There is no animal.')
    else:
        print('With animal: '+ac2)
    print('----------------------------------------------------------------')
    if ac2 in hostiles:
        print('There is a hostile '+ac2+ ' on your tile, do you want to attack?')
        attack = input()
        if attack == 'yes' or attack == 'true':
            takeDamage = random.randint(1,3)
            if takeDamage == 2:
                die = random.randint(1,2)
                if die == 2:
                    print('Uh oh, you died from fighting the '+ac2)
                    sys.exit()
                else:
                    damageTaken = random.randint(1,19)
                    print('Uh oh, you took '+str(damageTaken)+' damage from fighting the '+ac2+'.')
                    player.health -= damageTaken
            else:
                print('You kill the '+ac2)
                if player.food+10 >= 20:
                    
                    print ('You do not have enough room to carry all the food the '+ac2+' gave you, you carry all you can.')
                    player.food=20
                else:
                    print('The '+ac2+' gave you 10 food.')
                    player.food+=10
                

    main = input('> ')
    
    if player.food == 0:
        print('You died from starvation :(')
        sys.exit()
    if player.water == 0:
        print('You died of dehydration :(')
        sys.exit()
    if main == 'go north' or main == 'n' or main == 'north':
       player.move('north',grid)
    elif main == 'go south' or main == 's' or main == 'south':
        player.move('south',grid)
    elif main == 'go east' or main == 'e' or main == 'east':
        player.move('east',grid)
    elif main == 'go west' or main == 'w' or main == 'west':
        player.move('west',grid)

    elif main == 'collect water' or main == 'get water':
        playerBiome = biomes.checkBiome(grid[player.x][player.y])

        if player.water+3 >= 20:
            print('You do not have enough room to carry all the water you collect, you carry all you can.')
            player.water =20
        else:
        
            if playerBiome == 'Desert':
                map.Biomes.getWater(biomes.desert['name'],player.food)
                print('You cannot get water in a desert without some kind of well or irrigation canal.')

            elif playerBiome == 'Plains':
                
                print('You get 3 water.')
                player.water += 3
            elif playerBiome == 'Hills':
                
                print('You get 3 water.')
                player.water += 3
            elif playerBiome == 'Mountains':
                
                print('You get 3 water.')
                player.water += 3
            elif playerBiome == 'Sea':
                
                print('You fill up your water.')
                player.water = 20
            else:
                print('unk biome ' + playerBiome)

    elif main == 'collect berries' or main == 'get berries':
        playerBiome = biomes.checkBiome(grid[player.x][player.y])
        if player.food+3 >= 20:
            if playerBiome == 'Sea':
                placeholder = None
                #print('You cannot carry all the food you collect.')
                #player.food = 20
            else:
                print('You cannot carry all the food you collect, you carry all you can.')
                player.food = 20
        else:
        
            if playerBiome == 'Desert':
                print('You get 3 food')
                player.food +=3
                
            elif playerBiome == 'Plains':
                
                print('You get 3 food')
                player.food +=3
            elif playerBiome == 'Hills':
                
                print('You get 3 food')
                player.food +=3
            elif playerBiome == 'Mountains':
                
                print('You get 3 food')
                player.food +=3
            elif playerBiome == 'Sea':
                
                #add fishing rod code later
                print('You get 3 food')
                player.food +=3
            else:
                print('unk biome '+ playerBiome)
    elif main == 'quit':
        print('Thanks for playing!')
        sys.exit()
    elif main == 'how to play' or main == 'controls':
        print('https://gitlab.com/slappygoesslap/slap-adventure-game/-/wikis/Controls')

    elif main == 'collect meat' or main == 'kill animal':
        if animalArray[player.x][player.y] == 0:
            print('There is no animal on this tile.')
        else:
            if player.food+5 >= 20:
                player.food = 20
                animalArray[player.x][player.y] = 0
                print('You do not have enough room to carry all the food you\'ve collected, you carry all you can.')
            else:
                if 'Sword' not in player.inventory:
                    #animalArray[player.x][player.y] = 0
                    print('You could take damage or die from trying to kill this animal without a sword.')
                else:
                    animalArray[player.x][player.y] = 0
                    player.food +=5
                    print('You have collected 5 meat from this '+ac2)
    animalCycle+=1
    print('----------------------------------------------------------------')
    print('\n\n\n\n\n')